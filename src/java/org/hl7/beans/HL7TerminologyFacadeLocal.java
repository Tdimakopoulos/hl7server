/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import java.util.List;
import javax.ejb.Local;
import org.hl7.model.HL7Terminology;

/**
 *
 * @author tdim
 */
@Local
public interface HL7TerminologyFacadeLocal {

    void create(HL7Terminology hL7Terminology);

    void edit(HL7Terminology hL7Terminology);

    void remove(HL7Terminology hL7Terminology);

    HL7Terminology find(Object id);

    List<HL7Terminology> findAll();

    List<HL7Terminology> findRange(int[] range);

    int count();
    
}
