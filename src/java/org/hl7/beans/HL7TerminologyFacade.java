/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hl7.model.HL7Terminology;

/**
 *
 * @author tdim
 */
@Stateless
public class HL7TerminologyFacade extends AbstractFacade<HL7Terminology> implements HL7TerminologyFacadeLocal {
    @PersistenceContext(unitName = "HL7ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HL7TerminologyFacade() {
        super(HL7Terminology.class);
    }
    
}
