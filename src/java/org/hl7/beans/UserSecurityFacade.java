/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hl7.model.UserSecurity;

/**
 *
 * @author tdim
 */
@Stateless
public class UserSecurityFacade extends AbstractFacade<UserSecurity> implements UserSecurityFacadeLocal {
    @PersistenceContext(unitName = "HL7ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserSecurityFacade() {
        super(UserSecurity.class);
    }
    
}
