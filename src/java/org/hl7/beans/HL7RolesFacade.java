/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hl7.model.HL7Roles;

/**
 *
 * @author tdim
 */
@Stateless
public class HL7RolesFacade extends AbstractFacade<HL7Roles> implements HL7RolesFacadeLocal {
    @PersistenceContext(unitName = "HL7ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HL7RolesFacade() {
        super(HL7Roles.class);
    }
    
}
