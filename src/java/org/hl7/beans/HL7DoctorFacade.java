/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hl7.model.HL7Doctor;

/**
 *
 * @author tdim
 */
@Stateless
public class HL7DoctorFacade extends AbstractFacade<HL7Doctor> implements HL7DoctorFacadeLocal {
    @PersistenceContext(unitName = "HL7ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HL7DoctorFacade() {
        super(HL7Doctor.class);
    }
    
}
