/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import java.util.List;
import javax.ejb.Local;
import org.hl7.model.HL7Roles;

/**
 *
 * @author tdim
 */
@Local
public interface HL7RolesFacadeLocal {

    void create(HL7Roles hL7Roles);

    void edit(HL7Roles hL7Roles);

    void remove(HL7Roles hL7Roles);

    HL7Roles find(Object id);

    List<HL7Roles> findAll();

    List<HL7Roles> findRange(int[] range);

    int count();
    
}
