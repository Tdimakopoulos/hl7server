/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import java.util.List;
import javax.ejb.Local;
import org.hl7.model.HL7Associations;

/**
 *
 * @author tdim
 */
@Local
public interface HL7AssociationsFacadeLocal {

    void create(HL7Associations hL7Associations);

    void edit(HL7Associations hL7Associations);

    void remove(HL7Associations hL7Associations);

    HL7Associations find(Object id);

    List<HL7Associations> findAll();

    List<HL7Associations> findRange(int[] range);

    int count();
    
}
