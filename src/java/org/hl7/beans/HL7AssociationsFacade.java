/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hl7.model.HL7Associations;

/**
 *
 * @author tdim
 */
@Stateless
public class HL7AssociationsFacade extends AbstractFacade<HL7Associations> implements HL7AssociationsFacadeLocal {
    @PersistenceContext(unitName = "HL7ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HL7AssociationsFacade() {
        super(HL7Associations.class);
    }
    
}
