/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hl7.version.VersionControl;

/**
 *
 * @author tdim
 */
@Stateless
public class VersionControlFacade extends AbstractFacade<VersionControl> implements VersionControlFacadeLocal {
    @PersistenceContext(unitName = "HL7ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VersionControlFacade() {
        super(VersionControl.class);
    }
    
}
