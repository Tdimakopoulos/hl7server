/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hl7.model.HL7MedicalDevices;

/**
 *
 * @author tdim
 */
@Stateless
public class HL7MedicalDevicesFacade extends AbstractFacade<HL7MedicalDevices> implements HL7MedicalDevicesFacadeLocal {
    @PersistenceContext(unitName = "HL7ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HL7MedicalDevicesFacade() {
        super(HL7MedicalDevices.class);
    }
    
}
