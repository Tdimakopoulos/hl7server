/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import java.util.List;
import javax.ejb.Local;
import org.hl7.model.HL7MedicalDevices;

/**
 *
 * @author tdim
 */
@Local
public interface HL7MedicalDevicesFacadeLocal {

    void create(HL7MedicalDevices hL7MedicalDevices);

    void edit(HL7MedicalDevices hL7MedicalDevices);

    void remove(HL7MedicalDevices hL7MedicalDevices);

    HL7MedicalDevices find(Object id);

    List<HL7MedicalDevices> findAll();

    List<HL7MedicalDevices> findRange(int[] range);

    int count();
    
}
