/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hl7.model.HL7Observation;

/**
 *
 * @author tdim
 */
@Stateless
public class HL7ObservationFacade extends AbstractFacade<HL7Observation> implements HL7ObservationFacadeLocal {
    @PersistenceContext(unitName = "HL7ServerPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HL7ObservationFacade() {
        super(HL7Observation.class);
    }
    
}
