/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import java.util.List;
import javax.ejb.Local;
import org.hl7.model.UserSecurity;

/**
 *
 * @author tdim
 */
@Local
public interface UserSecurityFacadeLocal {

    void create(UserSecurity userSecurity);

    void edit(UserSecurity userSecurity);

    void remove(UserSecurity userSecurity);

    UserSecurity find(Object id);

    List<UserSecurity> findAll();

    List<UserSecurity> findRange(int[] range);

    int count();
    
}
