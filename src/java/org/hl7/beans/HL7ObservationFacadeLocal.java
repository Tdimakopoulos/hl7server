/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import java.util.List;
import javax.ejb.Local;
import org.hl7.model.HL7Observation;

/**
 *
 * @author tdim
 */
@Local
public interface HL7ObservationFacadeLocal {

    void create(HL7Observation hL7Observation);

    void edit(HL7Observation hL7Observation);

    void remove(HL7Observation hL7Observation);

    HL7Observation find(Object id);

    List<HL7Observation> findAll();

    List<HL7Observation> findRange(int[] range);

    int count();
    
}
