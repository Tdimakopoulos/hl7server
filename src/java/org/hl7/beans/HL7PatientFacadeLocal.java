/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import java.util.List;
import javax.ejb.Local;
import org.hl7.model.HL7Patient;

/**
 *
 * @author tdim
 */
@Local
public interface HL7PatientFacadeLocal {

    void create(HL7Patient hL7Patient);

    void edit(HL7Patient hL7Patient);

    void remove(HL7Patient hL7Patient);

    HL7Patient find(Object id);

    List<HL7Patient> findAll();

    List<HL7Patient> findRange(int[] range);

    int count();
    
}
