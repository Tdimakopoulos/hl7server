/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import java.util.List;
import javax.ejb.Local;
import org.hl7.model.HL7Doctor;

/**
 *
 * @author tdim
 */
@Local
public interface HL7DoctorFacadeLocal {

    void create(HL7Doctor hL7Doctor);

    void edit(HL7Doctor hL7Doctor);

    void remove(HL7Doctor hL7Doctor);

    HL7Doctor find(Object id);

    List<HL7Doctor> findAll();

    List<HL7Doctor> findRange(int[] range);

    int count();
    
}
