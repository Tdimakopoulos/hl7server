/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.beans;

import java.util.List;
import javax.ejb.Local;
import org.hl7.version.VersionControl;

/**
 *
 * @author tdim
 */
@Local
public interface VersionControlFacadeLocal {

    void create(VersionControl versionControl);

    void edit(VersionControl versionControl);

    void remove(VersionControl versionControl);

    VersionControl find(Object id);

    List<VersionControl> findAll();

    List<VersionControl> findRange(int[] range);

    int count();
    
}
