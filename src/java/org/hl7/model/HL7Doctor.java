/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class HL7Doctor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String surname;
    private String address1;
    private String address7;
    private String address2;
    private String address3;
    private String address4;
    private String address5;
    private String address6;
    private String email;
    private String phone;
    private String mobile;
    private String department;
    private String workinfo1;
    private String workinfo2;
    private String workinfo3;
    private String workinfo4;
    private String workinfo5;
    private String workinfo6;
    private String workinfo7;
    private String workinfo8;
    private String workinfo9;
    private String workinfo10;
    private String workinfo11;
    private String workinfo12;
    private String workinfo13;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HL7Doctor)) {
            return false;
        }
        HL7Doctor other = (HL7Doctor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.hl7.model.HL7Doctor[ id=" + id + " ]";
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * @param address1 the address1 to set
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * @return the address7
     */
    public String getAddress7() {
        return address7;
    }

    /**
     * @param address7 the address7 to set
     */
    public void setAddress7(String address7) {
        this.address7 = address7;
    }

    /**
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * @param address2 the address2 to set
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * @return the address3
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * @param address3 the address3 to set
     */
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    /**
     * @return the address4
     */
    public String getAddress4() {
        return address4;
    }

    /**
     * @param address4 the address4 to set
     */
    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    /**
     * @return the address5
     */
    public String getAddress5() {
        return address5;
    }

    /**
     * @param address5 the address5 to set
     */
    public void setAddress5(String address5) {
        this.address5 = address5;
    }

    /**
     * @return the address6
     */
    public String getAddress6() {
        return address6;
    }

    /**
     * @param address6 the address6 to set
     */
    public void setAddress6(String address6) {
        this.address6 = address6;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the workinfo1
     */
    public String getWorkinfo1() {
        return workinfo1;
    }

    /**
     * @param workinfo1 the workinfo1 to set
     */
    public void setWorkinfo1(String workinfo1) {
        this.workinfo1 = workinfo1;
    }

    /**
     * @return the workinfo2
     */
    public String getWorkinfo2() {
        return workinfo2;
    }

    /**
     * @param workinfo2 the workinfo2 to set
     */
    public void setWorkinfo2(String workinfo2) {
        this.workinfo2 = workinfo2;
    }

    /**
     * @return the workinfo3
     */
    public String getWorkinfo3() {
        return workinfo3;
    }

    /**
     * @param workinfo3 the workinfo3 to set
     */
    public void setWorkinfo3(String workinfo3) {
        this.workinfo3 = workinfo3;
    }

    /**
     * @return the workinfo4
     */
    public String getWorkinfo4() {
        return workinfo4;
    }

    /**
     * @param workinfo4 the workinfo4 to set
     */
    public void setWorkinfo4(String workinfo4) {
        this.workinfo4 = workinfo4;
    }

    /**
     * @return the workinfo5
     */
    public String getWorkinfo5() {
        return workinfo5;
    }

    /**
     * @param workinfo5 the workinfo5 to set
     */
    public void setWorkinfo5(String workinfo5) {
        this.workinfo5 = workinfo5;
    }

    /**
     * @return the workinfo6
     */
    public String getWorkinfo6() {
        return workinfo6;
    }

    /**
     * @param workinfo6 the workinfo6 to set
     */
    public void setWorkinfo6(String workinfo6) {
        this.workinfo6 = workinfo6;
    }

    /**
     * @return the workinfo7
     */
    public String getWorkinfo7() {
        return workinfo7;
    }

    /**
     * @param workinfo7 the workinfo7 to set
     */
    public void setWorkinfo7(String workinfo7) {
        this.workinfo7 = workinfo7;
    }

    /**
     * @return the workinfo8
     */
    public String getWorkinfo8() {
        return workinfo8;
    }

    /**
     * @param workinfo8 the workinfo8 to set
     */
    public void setWorkinfo8(String workinfo8) {
        this.workinfo8 = workinfo8;
    }

    /**
     * @return the workinfo9
     */
    public String getWorkinfo9() {
        return workinfo9;
    }

    /**
     * @param workinfo9 the workinfo9 to set
     */
    public void setWorkinfo9(String workinfo9) {
        this.workinfo9 = workinfo9;
    }

    /**
     * @return the workinfo10
     */
    public String getWorkinfo10() {
        return workinfo10;
    }

    /**
     * @param workinfo10 the workinfo10 to set
     */
    public void setWorkinfo10(String workinfo10) {
        this.workinfo10 = workinfo10;
    }

    /**
     * @return the workinfo11
     */
    public String getWorkinfo11() {
        return workinfo11;
    }

    /**
     * @param workinfo11 the workinfo11 to set
     */
    public void setWorkinfo11(String workinfo11) {
        this.workinfo11 = workinfo11;
    }

    /**
     * @return the workinfo12
     */
    public String getWorkinfo12() {
        return workinfo12;
    }

    /**
     * @param workinfo12 the workinfo12 to set
     */
    public void setWorkinfo12(String workinfo12) {
        this.workinfo12 = workinfo12;
    }

    /**
     * @return the workinfo13
     */
    public String getWorkinfo13() {
        return workinfo13;
    }

    /**
     * @param workinfo13 the workinfo13 to set
     */
    public void setWorkinfo13(String workinfo13) {
        this.workinfo13 = workinfo13;
    }
    
}
