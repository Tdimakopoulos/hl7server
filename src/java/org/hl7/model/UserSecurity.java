/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class UserSecurity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;
    private String password;
    private Long irole;
    private Long iperson;
    private String currentstatus;
    private String lastlogin;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserSecurity)) {
            return false;
        }
        UserSecurity other = (UserSecurity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.hl7.model.UserSecurity[ id=" + id + " ]";
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the irole
     */
    public Long getIrole() {
        return irole;
    }

    /**
     * @param irole the irole to set
     */
    public void setIrole(Long irole) {
        this.irole = irole;
    }

    /**
     * @return the iperson
     */
    public Long getIperson() {
        return iperson;
    }

    /**
     * @param iperson the iperson to set
     */
    public void setIperson(Long iperson) {
        this.iperson = iperson;
    }

    /**
     * @return the currentstatus
     */
    public String getCurrentstatus() {
        return currentstatus;
    }

    /**
     * @param currentstatus the currentstatus to set
     */
    public void setCurrentstatus(String currentstatus) {
        this.currentstatus = currentstatus;
    }

    /**
     * @return the lastlogin
     */
    public String getLastlogin() {
        return lastlogin;
    }

    /**
     * @param lastlogin the lastlogin to set
     */
    public void setLastlogin(String lastlogin) {
        this.lastlogin = lastlogin;
    }
    
}
