/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class HL7Terminology implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String hl7value;
    private String hl7key;
    private String hl7area;
    
    private String textfield1;
    private String textfield2;
    private String textfield3;
    private String textfield4;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HL7Terminology)) {
            return false;
        }
        HL7Terminology other = (HL7Terminology) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.hl7.model.HL7Terminology[ id=" + id + " ]";
    }

    /**
     * @return the hl7value
     */
    public String getHl7value() {
        return hl7value;
    }

    /**
     * @param hl7value the hl7value to set
     */
    public void setHl7value(String hl7value) {
        this.hl7value = hl7value;
    }

    /**
     * @return the hl7key
     */
    public String getHl7key() {
        return hl7key;
    }

    /**
     * @param hl7key the hl7key to set
     */
    public void setHl7key(String hl7key) {
        this.hl7key = hl7key;
    }

    /**
     * @return the hl7area
     */
    public String getHl7area() {
        return hl7area;
    }

    /**
     * @param hl7area the hl7area to set
     */
    public void setHl7area(String hl7area) {
        this.hl7area = hl7area;
    }

    /**
     * @return the textfield1
     */
    public String getTextfield1() {
        return textfield1;
    }

    /**
     * @param textfield1 the textfield1 to set
     */
    public void setTextfield1(String textfield1) {
        this.textfield1 = textfield1;
    }

    /**
     * @return the textfield2
     */
    public String getTextfield2() {
        return textfield2;
    }

    /**
     * @param textfield2 the textfield2 to set
     */
    public void setTextfield2(String textfield2) {
        this.textfield2 = textfield2;
    }

    /**
     * @return the textfield3
     */
    public String getTextfield3() {
        return textfield3;
    }

    /**
     * @param textfield3 the textfield3 to set
     */
    public void setTextfield3(String textfield3) {
        this.textfield3 = textfield3;
    }

    /**
     * @return the textfield4
     */
    public String getTextfield4() {
        return textfield4;
    }

    /**
     * @param textfield4 the textfield4 to set
     */
    public void setTextfield4(String textfield4) {
        this.textfield4 = textfield4;
    }
    
}
