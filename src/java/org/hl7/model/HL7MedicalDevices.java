/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class HL7MedicalDevices implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String networkip;
    private String networkport;
    private String ppnetworkip;
    private String ppnetworkport;
    private String name;
    private String serial;
    private String type;
    private String info1;
    private String info2;
    private String info3;
    private String info4;
    private String info5;
    private String info6;
    
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HL7MedicalDevices)) {
            return false;
        }
        HL7MedicalDevices other = (HL7MedicalDevices) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.hl7.model.HL7MedicalDevices[ id=" + id + " ]";
    }

    /**
     * @return the networkip
     */
    public String getNetworkip() {
        return networkip;
    }

    /**
     * @param networkip the networkip to set
     */
    public void setNetworkip(String networkip) {
        this.networkip = networkip;
    }

    /**
     * @return the networkport
     */
    public String getNetworkport() {
        return networkport;
    }

    /**
     * @param networkport the networkport to set
     */
    public void setNetworkport(String networkport) {
        this.networkport = networkport;
    }

    /**
     * @return the ppnetworkip
     */
    public String getPpnetworkip() {
        return ppnetworkip;
    }

    /**
     * @param ppnetworkip the ppnetworkip to set
     */
    public void setPpnetworkip(String ppnetworkip) {
        this.ppnetworkip = ppnetworkip;
    }

    /**
     * @return the ppnetworkport
     */
    public String getPpnetworkport() {
        return ppnetworkport;
    }

    /**
     * @param ppnetworkport the ppnetworkport to set
     */
    public void setPpnetworkport(String ppnetworkport) {
        this.ppnetworkport = ppnetworkport;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the info1
     */
    public String getInfo1() {
        return info1;
    }

    /**
     * @param info1 the info1 to set
     */
    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    /**
     * @return the info2
     */
    public String getInfo2() {
        return info2;
    }

    /**
     * @param info2 the info2 to set
     */
    public void setInfo2(String info2) {
        this.info2 = info2;
    }

    /**
     * @return the info3
     */
    public String getInfo3() {
        return info3;
    }

    /**
     * @param info3 the info3 to set
     */
    public void setInfo3(String info3) {
        this.info3 = info3;
    }

    /**
     * @return the info4
     */
    public String getInfo4() {
        return info4;
    }

    /**
     * @param info4 the info4 to set
     */
    public void setInfo4(String info4) {
        this.info4 = info4;
    }

    /**
     * @return the info5
     */
    public String getInfo5() {
        return info5;
    }

    /**
     * @param info5 the info5 to set
     */
    public void setInfo5(String info5) {
        this.info5 = info5;
    }

    /**
     * @return the info6
     */
    public String getInfo6() {
        return info6;
    }

    /**
     * @param info6 the info6 to set
     */
    public void setInfo6(String info6) {
        this.info6 = info6;
    }
    
}
