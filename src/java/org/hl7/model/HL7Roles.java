/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class HL7Roles implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String rolename;
    private String roletype;
    private String rolearea;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HL7Roles)) {
            return false;
        }
        HL7Roles other = (HL7Roles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.hl7.model.HL7Roles[ id=" + id + " ]";
    }

    /**
     * @return the rolename
     */
    public String getRolename() {
        return rolename;
    }

    /**
     * @param rolename the rolename to set
     */
    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    /**
     * @return the roletype
     */
    public String getRoletype() {
        return roletype;
    }

    /**
     * @param roletype the roletype to set
     */
    public void setRoletype(String roletype) {
        this.roletype = roletype;
    }

    /**
     * @return the rolearea
     */
    public String getRolearea() {
        return rolearea;
    }

    /**
     * @param rolearea the rolearea to set
     */
    public void setRolearea(String rolearea) {
        this.rolearea = rolearea;
    }
    
}
