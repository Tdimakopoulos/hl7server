/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class HL7Associations implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long patientid;
    private Long doctorid;
    private Long medicalid;
    
    private String info1;
    private String info2;
    private String info3;
    private String info4;
    private String info5;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HL7Associations)) {
            return false;
        }
        HL7Associations other = (HL7Associations) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.hl7.model.HL7Associations[ id=" + id + " ]";
    }

    /**
     * @return the patientid
     */
    public Long getPatientid() {
        return patientid;
    }

    /**
     * @param patientid the patientid to set
     */
    public void setPatientid(Long patientid) {
        this.patientid = patientid;
    }

    /**
     * @return the doctorid
     */
    public Long getDoctorid() {
        return doctorid;
    }

    /**
     * @param doctorid the doctorid to set
     */
    public void setDoctorid(Long doctorid) {
        this.doctorid = doctorid;
    }

    /**
     * @return the medicalid
     */
    public Long getMedicalid() {
        return medicalid;
    }

    /**
     * @param medicalid the medicalid to set
     */
    public void setMedicalid(Long medicalid) {
        this.medicalid = medicalid;
    }

    /**
     * @return the info1
     */
    public String getInfo1() {
        return info1;
    }

    /**
     * @param info1 the info1 to set
     */
    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    /**
     * @return the info2
     */
    public String getInfo2() {
        return info2;
    }

    /**
     * @param info2 the info2 to set
     */
    public void setInfo2(String info2) {
        this.info2 = info2;
    }

    /**
     * @return the info3
     */
    public String getInfo3() {
        return info3;
    }

    /**
     * @param info3 the info3 to set
     */
    public void setInfo3(String info3) {
        this.info3 = info3;
    }

    /**
     * @return the info4
     */
    public String getInfo4() {
        return info4;
    }

    /**
     * @param info4 the info4 to set
     */
    public void setInfo4(String info4) {
        this.info4 = info4;
    }

    /**
     * @return the info5
     */
    public String getInfo5() {
        return info5;
    }

    /**
     * @param info5 the info5 to set
     */
    public void setInfo5(String info5) {
        this.info5 = info5;
    }
    
}
