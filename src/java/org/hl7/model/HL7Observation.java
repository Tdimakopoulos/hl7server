/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class HL7Observation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long lpatient;
    
    private Long longvalue;
    private int intvalue;
    private String strvalue;
    
    private String name;
    private String stdname;
    
    private String strdate;
    private Long lngdate;
    private double dbldate;
    
    private String info1;
    private String info2;
    private String info3;
    private String info4;
    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HL7Observation)) {
            return false;
        }
        HL7Observation other = (HL7Observation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.hl7.model.HL7Observation[ id=" + id + " ]";
    }

    /**
     * @return the longvalue
     */
    public Long getLongvalue() {
        return longvalue;
    }

    /**
     * @param longvalue the longvalue to set
     */
    public void setLongvalue(Long longvalue) {
        this.longvalue = longvalue;
    }

    /**
     * @return the intvalue
     */
    public int getIntvalue() {
        return intvalue;
    }

    /**
     * @param intvalue the intvalue to set
     */
    public void setIntvalue(int intvalue) {
        this.intvalue = intvalue;
    }

    /**
     * @return the strvalue
     */
    public String getStrvalue() {
        return strvalue;
    }

    /**
     * @param strvalue the strvalue to set
     */
    public void setStrvalue(String strvalue) {
        this.strvalue = strvalue;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the stdname
     */
    public String getStdname() {
        return stdname;
    }

    /**
     * @param stdname the stdname to set
     */
    public void setStdname(String stdname) {
        this.stdname = stdname;
    }

    /**
     * @return the strdate
     */
    public String getStrdate() {
        return strdate;
    }

    /**
     * @param strdate the strdate to set
     */
    public void setStrdate(String strdate) {
        this.strdate = strdate;
    }

    /**
     * @return the lngdate
     */
    public Long getLngdate() {
        return lngdate;
    }

    /**
     * @param lngdate the lngdate to set
     */
    public void setLngdate(Long lngdate) {
        this.lngdate = lngdate;
    }

    /**
     * @return the dbldate
     */
    public double getDbldate() {
        return dbldate;
    }

    /**
     * @param dbldate the dbldate to set
     */
    public void setDbldate(double dbldate) {
        this.dbldate = dbldate;
    }

    /**
     * @return the info1
     */
    public String getInfo1() {
        return info1;
    }

    /**
     * @param info1 the info1 to set
     */
    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    /**
     * @return the info2
     */
    public String getInfo2() {
        return info2;
    }

    /**
     * @param info2 the info2 to set
     */
    public void setInfo2(String info2) {
        this.info2 = info2;
    }

    /**
     * @return the info3
     */
    public String getInfo3() {
        return info3;
    }

    /**
     * @param info3 the info3 to set
     */
    public void setInfo3(String info3) {
        this.info3 = info3;
    }

    /**
     * @return the info4
     */
    public String getInfo4() {
        return info4;
    }

    /**
     * @param info4 the info4 to set
     */
    public void setInfo4(String info4) {
        this.info4 = info4;
    }

    /**
     * @return the lpatient
     */
    public Long getLpatient() {
        return lpatient;
    }

    /**
     * @param lpatient the lpatient to set
     */
    public void setLpatient(Long lpatient) {
        this.lpatient = lpatient;
    }
    
}
