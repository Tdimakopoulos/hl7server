/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.ws.termin;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hl7.beans.HL7TerminologyFacadeLocal;
import org.hl7.model.HL7Terminology;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "TerminologyHL7")
@Stateless()
public class TerminologyHL7 {
    @EJB
    private HL7TerminologyFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "hL7Terminology") HL7Terminology hL7Terminology) {
        ejbRef.create(hL7Terminology);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "hL7Terminology") HL7Terminology hL7Terminology) {
        ejbRef.edit(hL7Terminology);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "hL7Terminology") HL7Terminology hL7Terminology) {
        ejbRef.remove(hL7Terminology);
    }

    @WebMethod(operationName = "find")
    public HL7Terminology find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<HL7Terminology> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<HL7Terminology> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
