/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.ws.assoc;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hl7.beans.HL7AssociationsFacadeLocal;
import org.hl7.model.HL7Associations;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "associationHL7")
@Stateless()
public class associationHL7 {
    @EJB
    private HL7AssociationsFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "hL7Associations") HL7Associations hL7Associations) {
        ejbRef.create(hL7Associations);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "hL7Associations") HL7Associations hL7Associations) {
        ejbRef.edit(hL7Associations);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "hL7Associations") HL7Associations hL7Associations) {
        ejbRef.remove(hL7Associations);
    }

    @WebMethod(operationName = "find")
    public HL7Associations find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<HL7Associations> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<HL7Associations> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
