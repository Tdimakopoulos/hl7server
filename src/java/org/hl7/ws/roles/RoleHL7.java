/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.ws.roles;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hl7.beans.HL7RolesFacadeLocal;
import org.hl7.model.HL7Roles;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "RoleHL7")
@Stateless()
public class RoleHL7 {
    @EJB
    private HL7RolesFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "hL7Roles") HL7Roles hL7Roles) {
        ejbRef.create(hL7Roles);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "hL7Roles") HL7Roles hL7Roles) {
        ejbRef.edit(hL7Roles);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "hL7Roles") HL7Roles hL7Roles) {
        ejbRef.remove(hL7Roles);
    }

    @WebMethod(operationName = "find")
    public HL7Roles find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<HL7Roles> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<HL7Roles> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
