/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.ws.obs;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hl7.beans.HL7ObservationFacadeLocal;
import org.hl7.model.HL7Observation;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "obshl7")
@Stateless()
public class obshl7 {
    @EJB
    private HL7ObservationFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "hL7Observation") HL7Observation hL7Observation) {
        ejbRef.create(hL7Observation);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "hL7Observation") HL7Observation hL7Observation) {
        ejbRef.edit(hL7Observation);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "hL7Observation") HL7Observation hL7Observation) {
        ejbRef.remove(hL7Observation);
    }

    @WebMethod(operationName = "find")
    public HL7Observation find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<HL7Observation> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<HL7Observation> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
