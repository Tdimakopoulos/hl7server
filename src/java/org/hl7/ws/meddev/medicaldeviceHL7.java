/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.ws.meddev;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hl7.beans.HL7MedicalDevicesFacadeLocal;
import org.hl7.model.HL7MedicalDevices;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "medicaldeviceHL7")
@Stateless()
public class medicaldeviceHL7 {
    @EJB
    private HL7MedicalDevicesFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "hL7MedicalDevices") HL7MedicalDevices hL7MedicalDevices) {
        ejbRef.create(hL7MedicalDevices);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "hL7MedicalDevices") HL7MedicalDevices hL7MedicalDevices) {
        ejbRef.edit(hL7MedicalDevices);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "hL7MedicalDevices") HL7MedicalDevices hL7MedicalDevices) {
        ejbRef.remove(hL7MedicalDevices);
    }

    @WebMethod(operationName = "find")
    public HL7MedicalDevices find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<HL7MedicalDevices> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<HL7MedicalDevices> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
