/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.ws.security;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hl7.beans.UserSecurityFacadeLocal;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "UserSecurity")
@Stateless()
public class UserSecurity {
    @EJB
    private UserSecurityFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "userSecurity") org.hl7.model.UserSecurity userSecurity) {
        ejbRef.create(userSecurity);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "userSecurity") org.hl7.model.UserSecurity userSecurity) {
        ejbRef.edit(userSecurity);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "userSecurity") org.hl7.model.UserSecurity userSecurity) {
        ejbRef.remove(userSecurity);
    }

    @WebMethod(operationName = "find")
    public org.hl7.model.UserSecurity find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<org.hl7.model.UserSecurity> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<org.hl7.model.UserSecurity> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
