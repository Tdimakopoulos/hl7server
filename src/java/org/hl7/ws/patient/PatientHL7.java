/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.ws.patient;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hl7.beans.HL7PatientFacadeLocal;
import org.hl7.model.HL7Patient;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "PatientHL7")
@Stateless()
public class PatientHL7 {
    @EJB
    private HL7PatientFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "hL7Patient") HL7Patient hL7Patient) {
        ejbRef.create(hL7Patient);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "hL7Patient") HL7Patient hL7Patient) {
        ejbRef.edit(hL7Patient);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "hL7Patient") HL7Patient hL7Patient) {
        ejbRef.remove(hL7Patient);
    }

    @WebMethod(operationName = "find")
    public HL7Patient find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<HL7Patient> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<HL7Patient> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
