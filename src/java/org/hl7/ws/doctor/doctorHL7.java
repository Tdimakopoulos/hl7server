/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.ws.doctor;

import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hl7.beans.HL7DoctorFacadeLocal;
import org.hl7.model.HL7Doctor;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "doctorHL7")
@Stateless()
public class doctorHL7 {
    @EJB
    private HL7DoctorFacadeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "create")
    @Oneway
    public void create(@WebParam(name = "hL7Doctor") HL7Doctor hL7Doctor) {
        ejbRef.create(hL7Doctor);
    }

    @WebMethod(operationName = "edit")
    @Oneway
    public void edit(@WebParam(name = "hL7Doctor") HL7Doctor hL7Doctor) {
        ejbRef.edit(hL7Doctor);
    }

    @WebMethod(operationName = "remove")
    @Oneway
    public void remove(@WebParam(name = "hL7Doctor") HL7Doctor hL7Doctor) {
        ejbRef.remove(hL7Doctor);
    }

    @WebMethod(operationName = "find")
    public HL7Doctor find(@WebParam(name = "id") Object id) {
        return ejbRef.find(id);
    }

    @WebMethod(operationName = "findAll")
    public List<HL7Doctor> findAll() {
        return ejbRef.findAll();
    }

    @WebMethod(operationName = "findRange")
    public List<HL7Doctor> findRange(@WebParam(name = "range") int[] range) {
        return ejbRef.findRange(range);
    }

    @WebMethod(operationName = "count")
    public int count() {
        return ejbRef.count();
    }
    
}
